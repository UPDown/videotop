//
//  VTAPI.h
//  VideoTop
//
//  Created by HDJ on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#import "YoukuAPIDefine.h"

//获取节目分类
//https://openapi.youku.com/v2/schemas/show/category.json
//根据类别获取节目
//https://openapi.youku.com/v2/shows/by_category.json?client_id=c8e797381a8e71ba&category=%E7%94%B5%E8%A7%86%E5%89%A7

//获取视频分类
//https://openapi.youku.com/v2/schemas/video/category.json
//根据类别获取视频
//https://openapi.youku.com/v2/videos/by_category.json?client_id=c8e797381a8e71ba&category=%E5%8A%A8%E6%BC%AB
@interface VTAPI : NSObject

//应用推荐
+ (void)GetRecommendAppListWithBlock:(void (^)(NSArray *gets, NSError *error))block;

//视频分类
+ (void)GetVideoCategoryWithBlock:(void (^)(NSArray *gets, NSError *error))block;

//根据分类获取视频
+ (void)GetVideoByCategory:(NSString *)category
                 withGenre:(NSString *)genre
                withPeriod:(NSString *)period
               withOrderby:(NSString *)orderby
                  withPage:(int)page
                 withCount:(int)count
           withResultBlock:(void (^)(NSArray *gets, NSError *error))block;

//获取相关视频
+ (void)GetVideoByRelated:(NSString *)videoId
                withCount:(int)count
          withResultBlock:(void (^)(NSArray *gets, NSError *error))block;


//搜索视频通过关键词
+ (void)searchVideoByKeyWord:(NSString *)keyword
                    withPage:(int)page
                   withCount:(int)count
             withResultBlock:(void (^)(NSArray *gets, NSError *error))block;
@end
