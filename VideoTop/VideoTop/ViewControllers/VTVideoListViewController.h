//
//  VTVideoListViewController.h
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTVideoListViewController : UIViewController
@property (nonatomic,strong) IBOutlet UITableView *tableView;

- (void)reloadData:(id)data;

//切换类别  catalogStr:大类别名称  subCatalogs:子类别数组 为nil 表示没有
-(void)changeCatalog:(NSString *)catalogStr subCatalog:(NSArray *)subCatalogs;
-(void)changeSubCatalog:(NSString *)subCatalog;
-(void)pushToPlayerWithDic:(NSDictionary *)playDic;

- (void)searchKeyword:(NSString *)key;
@end
