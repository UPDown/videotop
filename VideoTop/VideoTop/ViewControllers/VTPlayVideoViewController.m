//
//  VTPlayVideoViewController.m
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTPlayVideoViewController.h"
#import "VideoPlayerKit.h"
#import "VTAPI.h"
@interface VTPlayVideoViewController ()
{
    VideoPlayerKit *mVideoPlayer;
    NSDictionary *playVideoDic;
    
    UITableView *relatedtableView;
    NSArray *videoRelatedArray;
    
    
}
@end

@implementation VTPlayVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"";
    }
    return self;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [mVideoPlayer stop];
    mVideoPlayer.delegate = nil;
    [mVideoPlayer.view removeFromSuperview];
    mVideoPlayer = nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if(kDevIsIOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    float heigth = 240;
    if([[UIScreen mainScreen] bounds].size.height <500)
    {
        heigth =200;
    }
//    relatedtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, heigth, 0, self.view.frame.size.height-heigth)];
//    relatedtableView.dataSource = (id)self;
//    relatedtableView.delegate = (id)self;
//    [self.view addSubview:relatedtableView];
}

- (void)reachabilityChanged:(NSNotification*)note{
    /*
    MyReachability* curReach= [note object];
    
    NSParameterAssert([curReach isKindOfClass: [MyReachability class]]);
    NetworkStatus  NetworkStatusstatus = [curReach currentReachabilityStatus];
    
    switch (NetworkStatusstatus) {
        case NotReachable:
        {
            
        }
            break;
        case kReachableViaWiFi://kReachableViaWWAN
        {
            
        }
            break;
        case  kReachableViaWWAN:
        {
            [mVideoPlayer stop];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"网络状态改变" message:@"您当前使用的网络为3G/2G网络，是否继续播放？" delegate:self cancelButtonTitle:@"不了" otherButtonTitles:@"继续播放", nil];
            alert.tag =1002;
            [alert show];
            [alert release];
        }
            break;
        default:
            break;
    }
    */
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1002 && buttonIndex == 1) {
    
    }
}
- (void)changeInterfaceOrientation
{
    
    if(UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }
    else{
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
    }
    [UIViewController attemptRotationToDeviceOrientation];
}
-(void)playerhidden:(BOOL)hidde
{
    if(hidde)
    {
        if(UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) || mVideoPlayer.view.frame.size.width > [[UIScreen mainScreen] bounds].size.width)
        {
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
        }
        else{
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        }
    }
    else{
        if(UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
        {
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        }
        else{
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        }
    }
    
}

- (BOOL)shouldAutorotate{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

-(void)fff
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
        
        [UIView beginAnimations:nil context:nil];
        mVideoPlayer.view.frame = CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
        [UIView commitAnimations];
        
        if([mVideoPlayer controllerBarHidden])
        {
            [self performSelector:@selector(fff) withObject:nil afterDelay:0];
        }
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    else{
        [UIView beginAnimations:nil context:nil];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        
        float heigth = 240;
        if([[UIScreen mainScreen] bounds].size.height <500)
        {
            heigth =200;
        }
        
        mVideoPlayer.view.frame = CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, heigth);
        [UIView commitAnimations];
               [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    [mVideoPlayer changeInterfaceOrientation];
}


-(void)setPlayVideoDic:(NSDictionary *)dic
{
    NSString *idStr =[dic objectForKey:@"id"];
    int mode = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"videoMode"];
    NSString *playUrl = [self getVideoPlayUrlForId:idStr Mode:mode];
    NSURL *url = [NSURL URLWithString:[playUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    if(mVideoPlayer ==nil)
    {
        mVideoPlayer = [VideoPlayerKit videoPlayerWithContainingViewController:self optionalTopView:nil hideTopViewWithControls:NO];
        
        mVideoPlayer.delegate = (id)self;
        
        [self.view addSubview:mVideoPlayer.view];
        
        float heigth = 240;
        if([[UIScreen mainScreen] bounds].size.height <500)
        {
            heigth =200;
        }
        mVideoPlayer.videoPlayerView.frame =CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, heigth);
        
    }
    
    [mVideoPlayer playVideoWithTitle:dic[@"title"] URL:url videoID:idStr shareURL:nil isStreaming:NO playInFullScreen:NO];
//    [mVideoPlayer seekTo:0];  TODO 记录上次播放记录
    [mVideoPlayer setMode:mode];
}

-(NSString *)getVideoPlayUrlForId:(NSString *)idStr   Mode:(int)mode
{

    NSString *url =@"http://v.youku.com/player/getM3U8/vid/";
    long  time = (long)[[NSDate date] timeIntervalSince1970];

    
    NSString *hd3Url = @"";
    switch (mode) {
        case 0:  //高清
            hd3Url= [NSString stringWithFormat:@"%@/type/hd2/ts/%li/v.m3u8",idStr,time];
            break;
        case 1:  //标准
            hd3Url= [NSString stringWithFormat:@"%@/type/mp4/ts/%li/v.m3u8",idStr,time];
            break;
        case 2:  //流畅
            hd3Url=[NSString stringWithFormat:@"%@/type/flv/ts/%li/v.m3u8",idStr,time];
            break;
        default:
            break;
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",url,hd3Url];
    
    return urlStr;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)removeRecodeForKId:(NSString *)videoId
{
    
    NSString *fliePath = self.videoRecorde;
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithContentsOfFile:fliePath];
    if(dic == nil)
    {
        dic = [NSMutableDictionary dictionary];
    }
    [dic removeObjectForKey:videoId];
    
    [dic writeToFile:fliePath atomically:YES];
    
}
-(void)savePinRecordeValue:(float)value videoId:(NSString *)videoId
{
    //
    if(isfinite(value))
    {
        
        NSString *fliePath = self.videoRecorde;
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithContentsOfFile:fliePath];
        if(dic == nil)
        {
            dic = [NSMutableDictionary dictionary];
        }
        [dic setObject:[NSNumber numberWithFloat:value] forKey:videoId];
        
        [dic writeToFile:fliePath atomically:YES];
    }
}

-(float)readRecodeForKId:(NSString *)videoId
{
    NSString *fliePath = self.videoRecorde;
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithContentsOfFile:fliePath];
    if(dic == nil)
    {
        dic = [NSMutableDictionary dictionary];
    }
    float value = [[dic objectForKey:videoId] floatValue];
    return value;
}
- (void)trackEvent:(NSString *)event videoID:(NSString *)videoID title:(NSString *)title{
    
    if([event isEqualToString:kTrackEventVideoComplete])
    {
        NSLog(@"kTrackEventVideoComplete:%@",videoID);
        //播放完成
//        NSDictionary *strDic = [playingPin.rawText JSONValue];
//        if(strDic)
//        {
//            NSString *voideId = [strDic objectForKey:@"id"];
//            if (voideId) {
////                [self removeRecodeForKId:voideId];
//            }
//        }
    
        //播放下一个 TODO
   
    }
    else if([event isEqualToString:kTrackEventVideoLiveStart])
    {
        
        NSLog(@"kTrackEventVideoLiveStart:%@",videoID);
    }
}
-(void)videoChangeTo:(int)mode
{
    [[NSUserDefaults standardUserDefaults] setInteger:mode forKey:@"videoMode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *idStr =mVideoPlayer.videoIdStr;
    NSString *playUrl = [self getVideoPlayUrlForId:idStr Mode:mode];
    NSURL *url = [NSURL URLWithString:[playUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//
//    
//
    [mVideoPlayer saveSeek];
    [mVideoPlayer playVideoWithTitle:mVideoPlayer.currentVideoInfo[@"title"] URL: url videoID:idStr shareURL:nil isStreaming:NO playInFullScreen:NO];
    [mVideoPlayer setMode:mode];
    [mVideoPlayer reSeek];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    // Get the new view controller using [segue destinationViewController].
//    [segue destinationViewController];
//    // Pass the selected object to the new view controller.
//}


@end
