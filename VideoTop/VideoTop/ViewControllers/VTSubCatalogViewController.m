//
//  VTSubCatalogViewController.m
//  VideoTop
//
//  Created by helfy on 14-6-10.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTSubCatalogViewController.h"
#import "VTViewController.h"
@interface VTSubCatalogViewController ()
{
    UIScrollView *categoryView;
    NSArray *subCatalogArray;
}
@end

@implementation VTSubCatalogViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(kDevIsIOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setSubCatalogs:(NSArray *)subCatalogs
{
    subCatalogArray =subCatalogs;
    [self loadCategoryView];

}
-(void)loadCategoryView
{
    if (categoryView == nil) {
        categoryView = [[UIScrollView alloc] init];
        [self.view addSubview:categoryView];
    }
//    categoryView.backgroundColor = [UIColor lightGrayColor];
    for (UIButton *subButton in categoryView.subviews) {
        [subButton removeFromSuperview];
    }
    
    CGFloat width = 260.0f;
    
    CGFloat x = 20;
    CGFloat y = 20;
    
    for (int i=0;i<subCatalogArray.count;i++) {
        
        NSDictionary *category =[subCatalogArray objectAtIndex:i];
        
        NSString *name =[category objectForKey:@"label"];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        CGSize size = [name sizeWithFont:[UIFont boldSystemFontOfSize:14.0f]];
        
        if (x + size.width > width) {
            x = 20;
            y=y+45;
        }
        
        btn.frame =CGRectMake(x, y, size.width + 8.0f, 27.0f);
        [btn setTitle:name forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [btn addTarget:self action:@selector(changeCatalog:) forControlEvents:UIControlEventTouchUpInside];
        btn.backgroundColor = [UIColor redColor];

        
        btn.tag = i;
        [categoryView addSubview:btn];
        x += size.width + 20.0f;
    }
    y = y+50;
    if(y>self.view.frame.size.height)
    {
        
        categoryView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
        categoryView.contentSize = CGSizeMake(0, y);
    }
    else{
        
        categoryView.frame = CGRectMake(0, 0, 320, y);
    }


}
-(void)changeCatalog:(UIButton *)button
{
    VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    NSDictionary *dic = [subCatalogArray objectAtIndex:button.tag];
    [vc changeSubCatalog:dic[@"label"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
