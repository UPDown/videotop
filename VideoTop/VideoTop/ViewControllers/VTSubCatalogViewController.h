//
//  VTSubCatalogViewController.h
//  VideoTop
//
//  Created by helfy on 14-6-10.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTSubCatalogViewController : UIViewController


-(void)setSubCatalogs:(NSArray *)subCatalogs;
@end
