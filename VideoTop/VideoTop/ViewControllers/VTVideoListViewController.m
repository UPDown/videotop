//
//  VTVideoListViewController.m
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTVideoListViewController.h"
#import "VTVideoListTableViewCell.h"
#import "VTPlayVideoViewController.h"
#import "VTAPI.h"
#import "VTViewController.h"
@interface VTVideoListViewController ()
{
    NSMutableArray  *listArray;
    
    NSString *catalog;
    NSString *subCatalog;
}
@end

@implementation VTVideoListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    listArray =[[NSMutableArray alloc] init];

//    [self.tableView registerClass:[VTVideoListTableViewCell class] forCellReuseIdentifier:@"videoTableCellStr"];
    
    [self changeCatalog:@"随便看看" subCatalog:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [vc setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [super viewDidAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [vc setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
    [super viewWillDisappear:animated];
}

- (void)reloadData:(id)data
{
    [listArray removeAllObjects];
    
  
    
    [self.tableView reloadData];
}
-(void)changeSubCatalog:(NSString *)subCatalogstr
{
    self.title = subCatalogstr;
    
    [listArray removeAllObjects];
    subCatalog = [subCatalogstr copy];
    [VTAPI GetVideoByCategory:catalog withGenre:subCatalog withPeriod:nil withOrderby:nil withPage:1 withCount:20 withResultBlock:^(NSArray *gets, NSError *error) {
        if (!error) {
            if (gets && gets.count) {
                if ([[gets class] isSubclassOfClass:[NSArray class]]) {
                    [listArray addObjectsFromArray:gets];
                }
                else if ([[gets class] isSubclassOfClass:[NSDictionary class]]) {
                    [listArray addObject:gets];
                }
                [self.tableView reloadData];
            }
        }
        else{
            NSLog(@"error = %@",error);
        }
        
        
    }];
}

-(void)changeCatalog:(NSString *)catalogStr subCatalog:(NSArray *)subCatalogs
{
    self.title = catalogStr;
    catalog =[catalogStr copy];
    
    [listArray removeAllObjects];
    [VTAPI GetVideoByCategory:catalogStr withGenre:nil withPeriod:nil withOrderby:nil withPage:1 withCount:20 withResultBlock:^(NSArray *gets, NSError *error) {
        if (!error) {
            if (gets && gets.count) {
                if ([[gets class] isSubclassOfClass:[NSArray class]]) {
                    [listArray addObjectsFromArray:gets];
                }
                else if ([[gets class] isSubclassOfClass:[NSDictionary class]]) {
                    [listArray addObject:gets];
                }
                [self.tableView reloadData];
            }
        }
        else{
            NSLog(@"error = %@",error);
        }
     
        
    }];
    
    if(subCatalogs)
    {
        //开启右侧
        
    }
    else{
    
    }
}

- (void)searchKeyword:(NSString *)key
{
    self.title = key;
    [listArray removeAllObjects];
    
    [VTAPI searchVideoByKeyWord:key withPage:1 withCount:20 withResultBlock:^(NSArray *gets, NSError *error) {
        if (!error) {
            if (gets && gets.count) {
                [listArray addObjectsFromArray:gets];
            }
        }
        else{
        }
        
        if (self.tableView) {
            [self.tableView reloadData];
        }
    }];
}

- (BOOL)shouldAutorotate{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pushToPlayerWithDic:(NSDictionary *)playDic
{
    [self performSegueWithIdentifier:@"PushToPlay" sender:playDic];
}
#define krowDisplayCount 2
-(NSArray *)cutArrayWithBeginIndex:(int)startIndex length:(int)length targetArray:(NSArray *)targetArray
{
    NSArray *subArray =nil;
    if(startIndex < targetArray.count)
    {
        if(startIndex+length >= targetArray.count)
        {
            subArray = [targetArray subarrayWithRange:NSMakeRange(startIndex, targetArray.count-startIndex)];
        }
        else{
            subArray = [targetArray subarrayWithRange:NSMakeRange(startIndex, length)];
        }
    }
    return subArray;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    if (listArray.count) {
        int c = listArray.count;
        count = c/krowDisplayCount+(c%krowDisplayCount?1:0) + 1;
        //        count = count+1;
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return 210;
    
    NSInteger count =  listArray.count;
    if(indexPath.row < count/krowDisplayCount+(count%krowDisplayCount?1:0)+1)
    {
        return 140;
    }
    return 70;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VTVideoListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videoTableCellStr" forIndexPath:indexPath];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuTableCellStr"];
//    }
    
//    NSDictionary *dic = [listArray objectAtIndex:indexPath.row];
    if(indexPath.row ==0)
    {
        cell.cellHeigth = 210;
        [cell setShowDicArray:[self cutArrayWithBeginIndex:indexPath.row*krowDisplayCount length:1 targetArray:listArray]];
    }
    else{
        cell.cellHeigth = 140;
        NSMutableArray *subArray =[NSMutableArray arrayWithArray: [self cutArrayWithBeginIndex:indexPath.row*krowDisplayCount-1 length:krowDisplayCount targetArray:listArray]];
        
        if(subArray.count <2)
        {
            [subArray addObject:[NSNull null]];
        }
        
        
        [cell setShowDicArray:subArray];
    }
 
    [cell reloadView];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    VTPlayVideoViewController *viewController =[segue destinationViewController];
    [viewController setPlayVideoDic:sender];
//    viewController.title = @"";
}


@end
