//
//  VTMeunViewController.m
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTMeunViewController.h"
#import "VTMeunTableViewCell.h"
#import "VTAPI.h"
#import "VTViewController.h"
#import "MBProgressHUD.h"
#import "ERMeunCellObj.h"
#import "CAppListCell.h"
#define kNetWorkModeKey @"NetWorkMode"

@interface VTMeunViewController ()
{
    NSMutableArray  *menuCellObjArray;
    NSMutableArray  *listArray;
    NSMutableArray  *appListArray;
    
    UIView          *categoryView;
    UISearchBar     *searchBar;

}
@end

@implementation VTMeunViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)reSetMuenCellObj
{
    [menuCellObjArray removeAllObjects];
    
    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[ERMeunCellObj cellObjWith:@"搜索" level:0 action:@selector(showSearchBar)]];
    [array addObject:[ERMeunCellObj cellObjWith:@"3G模式" level:1 action:nil]];
    [menuCellObjArray addObject:array];
    
    array = [NSMutableArray array];
    [array addObject:[ERMeunCellObj cellObjWith:@"类别" level:0 action:nil]];
    [menuCellObjArray addObject:array];
    
    array = [NSMutableArray array];
    [array addObject:[ERMeunCellObj cellObjWith:@"反馈" level:1 action:@selector(feedBackAction)]];
    [array addObject:[ERMeunCellObj cellObjWith:@"评分" level:1 action:@selector(evaluateAction)]];
    [menuCellObjArray addObject:array];
    
//    array = [NSMutableArray array];
//    [array addObject:[ERMeunCellObj cellObjWith:@"推荐" level:0 action:@selector(feedBackAction)]];
//    [menuCellObjArray addObject:array];
    
    if(self.tableView.visibleCells.count>0)
    {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    else{
        [self.tableView reloadData];
    }
}

- (void)loadView
{
    [super loadView];
    
    listArray = [[NSMutableArray alloc] init];
    menuCellObjArray = [[NSMutableArray alloc] init];
    appListArray = [[NSMutableArray alloc] init];
    
    [VTAPI GetVideoCategoryWithBlock:^(NSArray *gets, NSError *error) {
        if (!error) {
            if (gets && gets.count) {
                [listArray addObjectsFromArray:gets];
            }
        }
        else{
        }
        
        if (self.tableView) {
            [self.tableView reloadData];
        }
    }];
    
    
    [VTAPI GetRecommendAppListWithBlock:^(NSArray *gets, NSError *error) {
        [appListArray removeAllObjects];
        
        if (!error) {
            
            if ([[gets class] isSubclassOfClass:[NSArray class]]) {
                [appListArray addObjectsFromArray:gets];
            }
            else if ([[gets class] isSubclassOfClass:[NSDictionary class]]) {
                [appListArray addObject:gets];
            }
            
            if (appListArray && appListArray.count) {
                
                if (menuCellObjArray.count>=4) {
                    [menuCellObjArray removeLastObject];
                }
                NSMutableArray *array = [NSMutableArray array];
                for (NSDictionary *dic in appListArray) {
                    [array addObject:dic];
                }
                [menuCellObjArray addObject:array];
            }

        }
        else{
        }
        
        if (self.tableView) {
            [self.tableView reloadData];
        }
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB(242, 242, 242, 1);
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
    [self reSetMuenCellObj];
    
    
    // Do any additional setup after loading the view.
//    [self.tableView registerClass:[VTMeunTableViewCell class] forCellReuseIdentifier:@"menuTableCellStr"];
}

-(void)loadCategoryView
{
    if (categoryView == nil) {
        categoryView = [[UIView alloc] init];
    }
    categoryView.backgroundColor = [UIColor whiteColor];
    for (UIButton *subButton in categoryView.subviews) {
        [subButton removeFromSuperview];
    }
    CGFloat width = 260.0f;
    CGFloat x = 20;
    CGFloat y = 20;

    for (int i=0;i<listArray.count;i++) {
        NSDictionary *category =[listArray objectAtIndex:i];
        NSString *name =[category objectForKey:@"label"];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        CGSize size = [name sizeWithFont:[UIFont boldSystemFontOfSize:14.0f]];
        if (x + size.width > width) {
            x = 20;
            y=y+45;
        }
        
        btn.frame =CGRectMake(x, y, size.width + 8.0f, 27.0f);
        [btn setTitle:name forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [btn addTarget:self action:@selector(changeCatalog:) forControlEvents:UIControlEventTouchUpInside];
        btn.backgroundColor = [UIColor blackColor];
        
//        [btn setBackgroundImage:[[UIImage imageNamed:@"menu-section-button.png"] stretchableImageWithLeftCapWidth:6.0f topCapHeight:13.0f] forState:UIControlStateNormal];
//        [btn setBackgroundImage:[[UIImage imageNamed:@"menu-section-button-pressed.png"] stretchableImageWithLeftCapWidth:6.0f topCapHeight:13.0f] forState:UIControlStateSelected];
        
        btn.tag = i;
        [categoryView addSubview:btn];
        x += size.width + 20.0f;
    }
    y = y+50;
    categoryView.frame = CGRectMake(0, 0, 320, y);
}


- (void)showSearchBar{
    
    if (searchBar == nil) {
        searchBar = [[UISearchBar alloc] init];
        //        searchBar.frame = CGRectMake(0, 31, 320, 44);
        searchBar.frame = CGRectMake(0, -44, 280, 44);
        searchBar.alpha = 1;
        searchBar.delegate = (id)self;
        searchBar.placeholder = @"搜搜搜～～～";
        searchBar.barStyle = UIBarStyleDefault;
        searchBar.backgroundColor = [UIColor clearColor];
        [self.view addSubview:searchBar];
    }
    
    UIView *bgView = [self.view viewWithTag:32320];
    if (nil == bgView) {
        bgView = [[UIView alloc] init];
        bgView.alpha = 0;
        bgView.tag = 32320;
        bgView.frame = self.view.bounds;
        [bgView setBackgroundColor:[UIColor blackColor]];
        [self.view addSubview:bgView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSearchBar)];
        [bgView addGestureRecognizer:tap];
        [self.view bringSubviewToFront:searchBar];
    }
    
    BOOL show = bgView.alpha==0?YES:NO;
    
    if (show) {
        searchBar.alpha = 1;
    }
    
//    [[[UIApplication sharedApplication] delegate] setIsSearching:bgView.alpha==0?YES:NO];
    show?[searchBar becomeFirstResponder]:[searchBar resignFirstResponder];
    show?searchBar.text = nil:searchBar.text;
    [UIView animateWithDuration:0.3 animations:^{
        searchBar.frame = show?CGRectMake(0, (kDevIsIOS7?20:0), 280, 44):CGRectMake(0, -44, 280, 44);

        bgView.alpha = show?0.4:0;
        
    } completion:^(BOOL finished) {
        
        if (!show) {
            searchBar.alpha = 0;
        }
        
    }];
}


- (void)changeCatalog:(UIButton *)btn{
        
    NSInteger index = btn.tag;
    
    if (listArray && listArray.count) {
        NSDictionary *objDic = [listArray objectAtIndex:index];
        NSArray *genresArray = [objDic objectForKey:@"genres"];
        VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
        [vc changeCatalog:[objDic objectForKey:@"label"] subCatalog:genresArray];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - -UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = [[menuCellObjArray objectAtIndex:indexPath.section]  objectAtIndex:indexPath.row];
    if ([[obj class] isSubclassOfClass:[ERMeunCellObj class]]) {
        ERMeunCellObj *cellObj = (ERMeunCellObj *)obj;

        if([cellObj.title isEqualToString:@"类别"]) {
            [self loadCategoryView];
            return categoryView.frame.size.height;
        }
        return 40;
    }
    else if ([[obj class] isSubclassOfClass:[NSDictionary class]]){
        return 50;
    }
    return 40;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return menuCellObjArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[menuCellObjArray objectAtIndex:section] count];
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
////    NSString *sectionName = [self tableView:tableView titleForHeaderInSection:section];
//    NSDictionary *objDic = [listArray objectAtIndex:section];
//    NSString *sectionName  =  [objDic objectForKey:@"label"];
//    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
//    bgView.backgroundColor = [UIColor whiteColor];
//    
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 300, 50)];
//    titleLabel.textColor = [UIColor blackColor];
//    titleLabel.font = [UIFont boldSystemFontOfSize:20.0];
//    titleLabel.backgroundColor = [UIColor clearColor];
//    titleLabel.text = sectionName;
//    [bgView addSubview:titleLabel];
//    
//    UIButton* eButton = [[UIButton alloc] init];
//    
//    //按钮填充整个视图
//    eButton.frame = bgView.frame;
//    eButton.backgroundColor = [UIColor clearColor];
//    [eButton addTarget:self action:@selector(expandButtonClicked:)
//      forControlEvents:UIControlEventTouchUpInside];
//    eButton.tag = section;//把节号保存到按钮tag，以便传递到expandButtonClicked方法
//    [bgView addSubview:eButton];
//    return bgView;
//}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = [[menuCellObjArray objectAtIndex:indexPath.section]  objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = nil;
    if ([[obj class] isSubclassOfClass:[ERMeunCellObj class]]) {
        static NSString *CellIdentifier = @"menuTableCellStr";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        ERMeunCellObj *cellObj = (ERMeunCellObj *)obj;
        
        UISwitch *switchView = ( UISwitch *)[cell.contentView viewWithTag:1000];
        
        if([cellObj.title isEqualToString:@"3G模式"])
        {
            cell.textLabel.text = cellObj.title;
            
            if(switchView == nil)
            {
                switchView = [[UISwitch alloc] initWithFrame:CGRectMake(180+(kDevIsIOS7?20:0), 5, 40, 30)];
                switchView.tag = 1000;
                [cell.contentView addSubview:switchView];
                [switchView addTarget:self action:@selector(changeSaveMode) forControlEvents:UIControlEventValueChanged];
                BOOL saveMode = [[NSUserDefaults standardUserDefaults] boolForKey:kNetWorkModeKey];
                switchView.on =saveMode;
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            switchView.hidden = NO;
        }
        else if ([cellObj.title isEqualToString:@"类别"])
        {
            [cell addSubview:categoryView];
        }
        else{
            cell.textLabel.text = cellObj.title;
            switchView.hidden = YES;
        }
    }
    else if ([[obj class] isSubclassOfClass:[NSDictionary class]]){
        static NSString *CellIdentifier = @"CAppListCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CAppListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            if(kDevIsIOS7)
            {
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            }
            else
            {
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
            }
            cell.textLabel.font = [UIFont systemFontOfSize:14];
        }
        
        [(CAppListCell *)cell setShowDic:obj];

    }

    return cell;
    
    /**
     *  ///
     
    //在storyBoard中已经注册 “menuTableCellStr”  可以不用初始化
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuTableCellStr" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 0:
        {
            //搜索
            cell.textLabel.text = [menuCellObjArray objectAtIndex:0];
        }
            break;
        case 1:
        {
            UISwitch *switchView = ( UISwitch *)[cell.contentView viewWithTag:1000];
            
            if(switchView == nil)
            {
                switchView = [[UISwitch alloc] initWithFrame:CGRectMake(180+(kDevIsIOS7?20:0), 5, 40, 30)];
                switchView.tag = 1000;
                [cell.contentView addSubview:switchView];
                [switchView addTarget:self action:@selector(changeSaveMode) forControlEvents:UIControlEventValueChanged];
                BOOL saveMode = [[NSUserDefaults standardUserDefaults] boolForKey:kNetWorkModeKey];
                switchView.on =saveMode;
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            switchView.hidden = NO;

        }
        case kCategoryIndex:
        {
            //栏目
            [cell addSubview:categoryView];
        }
            
        default:
            break;
    }
     */
    
  
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    id obj = [[menuCellObjArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if ([[obj class] isSubclassOfClass:[ERMeunCellObj class]]) {
        ERMeunCellObj *cellObj = (ERMeunCellObj *)obj;
        if (cellObj.action)
        {
            [self performSelector:cellObj.action];
        }
    }
    else if ([[obj class] isSubclassOfClass:[NSDictionary class]]){
    
        NSString * urlStr = [obj objectForKey:@"downUrl"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }
}


-(void)changeSaveMode
{
    BOOL saveMode = [[NSUserDefaults standardUserDefaults] boolForKey:kNetWorkModeKey];
    saveMode = !saveMode;
    [[NSUserDefaults standardUserDefaults] setBool:saveMode forKey:kNetWorkModeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    MBProgressHUD* HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
    [self.view.window addSubview:HUD];
    HUD.dimBackground = YES;
    HUD.mode = MBProgressHUDModeText;
    
    if(saveMode)  //TODO 提示作用
    {
        HUD.labelText = @"切图3G模式";
    }
    else{
        HUD.labelText = @"切图普通模式";
    }
    [HUD show:YES];
    [HUD hide:YES afterDelay:1];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - -search
- (void)search:(NSString *)str{
    
    VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc searchKeyword:str];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    if (_searchBar.text && [_searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@""]) {
        
        [self search:_searchBar.text];
    }
    [self showSearchBar];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self showSearchBar];
}



#pragma mark - -action
- (void)feedBackAction
{
    VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc feedBackAction];
}

//好评
- (void)evaluateAction
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ReviewURL]];
}

@end
