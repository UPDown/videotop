//
//  VTPlayVideoViewController.h
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTPlayVideoViewController : UIViewController
-(void)setPlayVideoDic:(NSDictionary *)dic;

@property (nonatomic,retain) NSString *videoRecorde;
@end
