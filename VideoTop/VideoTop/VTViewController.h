//
//  VTViewController.h
//  VideoTop
//
//  Created by HDJ on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "VTMeunViewController.h"
#import "VTVideoListViewController.h"

@interface VTViewController : MMDrawerController
@property (nonatomic,strong)  IBOutlet UIViewController   * meunViewController;
@property (nonatomic,strong)  IBOutlet UIViewController  * videoListViewController;
@property (nonatomic,strong)  IBOutlet UIViewController  * subCatalogViewController;


-(void)changeCatalog:(NSString *)catalogStr subCatalog:(NSArray *)subCatalogs;

//subCatalogViewController 回调
-(void)changeSubCatalog:(NSString *)subcCatalogStr;


-(void)pushToPlayerWithDic:(NSDictionary *)playDic;

- (void)searchKeyword:(NSString *)key;

- (void)feedBackAction;
@end
