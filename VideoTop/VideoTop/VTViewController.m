//
//  VTViewController.m
//  VideoTop
//
//  Created by HDJ on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTViewController.h"
#import "VTSubCatalogViewController.h"
#import "VTVideoListViewController.h"
#import "UMFeedbackViewController.h"
#import "VTAPI.h"

@interface VTViewController ()

@end

@implementation VTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self performSegueWithIdentifier:@"VTAddSegue1" sender:nil];
    [self performSegueWithIdentifier:@"VTAddSegue2" sender:nil];
    [self performSegueWithIdentifier:@"VTAddSegue3" sender:nil];
    
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:self.videoListViewController];
    
    [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
    
    UINavigationController * leftSideNavController = [[UINavigationController alloc] initWithRootViewController:self.meunViewController];

//    if(kDevIsIOS7){
//        UINavigationController * leftSideNavController = [[UINavigationController alloc] initWithRootViewController:self.meunViewController];
//		[leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
// 
//    }
//    else{
//        UINavigationController * leftSideNavController = [[UINavigationController alloc] initWithRootViewController:leftViewController];
//		[leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
//        
//    }
    self.centerViewController=navigationController;
    self.leftDrawerViewController=leftSideNavController;

    
    [self setRestorationIdentifier:@"MMDrawer"];
    [self setMaximumLeftDrawerWidth:280];
    //    [self.drawerController setShowsShadow:YES];
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    self.shouldStretchDrawer = NO;

	// Do any additional setup after loading the view, typically from a nib.
//    
//    self.meunViewController = [[VTMeunViewController alloc] initWithNibName:nil bundle:nil];
//    
//    self.videoListViewController = [[VTVideoListViewController alloc] initWithNibName:nil bundle:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass:[VTVideoListViewController class]])
    {
        self.videoListViewController =segue.destinationViewController;
      
    }
    else  if([segue.destinationViewController isKindOfClass:[VTMeunViewController class]])
    {
    
      self.meunViewController =segue.destinationViewController;
       
    }
    else  if([segue.destinationViewController isKindOfClass:[VTSubCatalogViewController class]])
    {
        
        self.subCatalogViewController =segue.destinationViewController;
        
    }
}
-(void)changeSubCatalog:(NSString *)subcCatalogStr
{
    [(VTVideoListViewController *)self.videoListViewController changeSubCatalog:subcCatalogStr];
//    __block VTViewController * sweakSelf = (VTViewController *) self;
    [self setCenterViewController:self.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {

    }];
}
-(void)changeCatalog:(NSString *)catalogStr subCatalog:(NSArray *)subCatalogs
{

    [(VTVideoListViewController *)self.videoListViewController changeCatalog:catalogStr subCatalog:subCatalogs];
    __block VTViewController * sweakSelf = (VTViewController *) self;
    [self setCenterViewController:self.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        
        if(subCatalogs.count>0 && subCatalogs)
        {
            UINavigationController * rigthSideNavController = [[UINavigationController alloc] initWithRootViewController:sweakSelf.subCatalogViewController];
            

            sweakSelf.rightDrawerViewController =rigthSideNavController;
            
            [(VTSubCatalogViewController *)sweakSelf.subCatalogViewController setSubCatalogs:subCatalogs];
            
            [sweakSelf bouncePreviewForDrawerSide:MMDrawerSideRight completion:^(BOOL finished) {
                            
            }];
        }
        else{
             sweakSelf.rightDrawerViewController = nil;
         
        }
    }];
}


- (void)feedBackAction
{
    [self setCenterViewController:self.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        
        UMFeedbackViewController *feedbackViewController = [[UMFeedbackViewController alloc] initWithNibName:@"UMFeedbackViewController" bundle:nil];
        feedbackViewController.appkey = UMENG_APPKEY;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:feedbackViewController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }];
}

-(void)pushToPlayerWithDic:(NSDictionary *)playDic
{
    [(VTVideoListViewController *)self.videoListViewController pushToPlayerWithDic:playDic];
}



- (BOOL)shouldAutorotate{
    return [[(UINavigationController *)self.centerViewController visibleViewController] shouldAutorotate];
}
- (NSUInteger)supportedInterfaceOrientations
{
  return [[(UINavigationController *)self.centerViewController visibleViewController] supportedInterfaceOrientations];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
   return [[(UINavigationController *)self.centerViewController visibleViewController] shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}


- (void)searchKeyword:(NSString *)key
{
    [(VTVideoListViewController *)self.videoListViewController searchKeyword:key];
    //    __block VTViewController * sweakSelf = (VTViewController *) self;
    [self setCenterViewController:self.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        
    }];
}


@end
