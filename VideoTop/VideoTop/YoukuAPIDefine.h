//
//  YoukuAPIDefine.h
//  MVideo
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 helfy . All rights reserved.
//

#ifndef MVideo_YoukuAPIDefine_h
#define MVideo_YoukuAPIDefine_h

#define kYoukuClient_id @"c8e797381a8e71ba"
//类别 top榜   参数：?client_id=c8e797381a8e71ba&category=%E7%BB%BC%E8%89%BA&headnum=1&tailnum=1
#define kYoukuTop_unite @"https://openapi.youku.com/v2/searches/show/top_unite.json"

//热门搜索
#define kYoukuSearches_top @"https://openapi.youku.com/v2/searches/keyword/top.json"  //?client_id=c8e797381a8e71ba
#endif

//视频分类
#define kVideoCategory  @"https://openapi.youku.com/v2/schemas/video/category.json"

//根据分类获取视频
#define kVideosBy_category  @"https://openapi.youku.com/v2/videos/by_category.json"

//视频相关
#define kVideosBy_related @"https://openapi.youku.com/v2/videos/by_related.json"

//搜索视频通过关键词
#define kVideoBy_keyword    @"https://openapi.youku.com/v2/searches/video/by_keyword.json"