//
//  VTAPI.m
//  VideoTop
//
//  Created by HDJ on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTAPI.h"

@implementation VTAPI

//应用推荐
+ (void)GetRecommendAppListWithBlock:(void (^)(NSArray *gets, NSError *error))block
{
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    
    NSString *urlString =  [NSString stringWithFormat:@"http://helfyapplist.duapp.com/recommend.php?appName=%@",[appName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        
        if (block) {
            block(responseObject,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block([NSArray array],error);
        }
    }];
}


//视频分类
+ (void)GetVideoCategoryWithBlock:(void (^)(NSArray *gets, NSError *error))block
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:kVideoCategory parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        
        if ([[responseObject class] isSubclassOfClass:[NSArray class]]) {
            [resultArray addObjectsFromArray:responseObject];
        }
        else if ([[responseObject class] isSubclassOfClass:[NSDictionary class]]){
            NSArray *array = [responseObject objectForKey:@"categories"];
            [resultArray addObjectsFromArray:array];
        }
        
        if (block) {
            block(resultArray,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block([NSArray array],error);
        }
    }];
}

//根据分类获取视频
+ (void)GetVideoByCategory:(NSString *)category
                 withGenre:(NSString *)genre
                withPeriod:(NSString *)period
               withOrderby:(NSString *)orderby
                  withPage:(int)page
                 withCount:(int)count
           withResultBlock:(void (^)(NSArray *gets, NSError *error))block
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kYoukuClient_id forKey:@"client_id"];
    if (category) {
        [dic setObject:category forKey:@"category"];
    }
    if (genre) {
        [dic setObject:genre forKey:@"genre"];
    }
    if (period) {
        [dic setObject:period forKey:@"period"];
    }
    if (orderby) {
        [dic setObject:orderby forKey:@"orderby"];
    }
    
    [dic setObject:[NSNumber numberWithInt:page] forKey:@"page"];
    [dic setObject:[NSNumber numberWithInt:count] forKey:@"count"];
    
    
    [manager GET:kVideosBy_category parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *resultArray = [responseObject objectForKey:@"videos"];
        
        if (block) {
            block(resultArray,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block([NSArray array],error);
        }
    }];
}

//根据视频ID获取相关视频kVideosBy_related
+ (void)GetVideoByRelated:(NSString *)videoId
                 withCount:(int)count
           withResultBlock:(void (^)(NSArray *gets, NSError *error))block
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kYoukuClient_id forKey:@"client_id"];
    [dic setObject:videoId forKey:@"video_id"];
    if(count ==0)count=20;
    [dic setObject:[NSNumber numberWithInt:count] forKey:@"count"];
    
    [manager GET:kVideosBy_related parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *resultArray = [responseObject objectForKey:@"videos"];
        
        if (block) {
            block(resultArray,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block([NSArray array],error);
        }
    }];
}

+ (void)searchVideoByKeyWord:(NSString *)keyword
                    withPage:(int)page
                   withCount:(int)count
             withResultBlock:(void (^)(NSArray *gets, NSError *error))block{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:kYoukuClient_id forKey:@"client_id"];
    [dic setObject:keyword forKey:@"keyword"];
 
    [dic setObject:[NSNumber numberWithInt:page] forKey:@"page"];
    [dic setObject:[NSNumber numberWithInt:count] forKey:@"count"];
    
    [manager GET:kVideoBy_keyword parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *resultArray = [responseObject objectForKey:@"videos"];
        
        if (block) {
            block(resultArray,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (block) {
            block([NSArray array],error);
        }
    }];
}

@end
