//
//  VTVideoListTableViewCell.h
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTVideoListTableViewCell : UITableViewCell
{
   NSMutableArray *singleViews;
}
@property (nonatomic,strong) NSArray *showDicArray;
@property (nonatomic,assign) float cellHeigth;
@property (nonatomic,assign) int disPlayCount;//显示几个

-(void)reloadView;
@end
