//
//  VTSingleVideoView.m
//  VideoTop
//
//  Created by helfy on 14-6-10.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTSingleVideoView.h"
#import "VTViewController.h"
#import "UIImageView+WebCache.h"
#import "VTStringConver.h"

@implementation VTSingleVideoView
{
    UIImageView *thumbailImageView;  //缩略图   对应key thumbnail_v2
    UILabel *publishDataLabel;      //发布时间  对应key published
    UILabel *durationLabel;         //播放时间  对应key duration
    UILabel *playerCountLabel;      //播放次数  对应key view_count
    UILabel *titleLabe;
    
    UIView *imageCoverView;
    NSDictionary *dicData;

}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor=[UIColor whiteColor];
        titleLabe = [[UILabel alloc] initWithFrame:self.bounds];
        titleLabe.numberOfLines =0;
//        titleLabe.adjustsFontSizeToFitWidth = YES;
        
        titleLabe.font =[UIFont systemFontOfSize:12];
        [self addSubview:titleLabe];
        
        thumbailImageView = [[UIImageView alloc] initWithFrame:CGRectInset(self.bounds, 5, 5)];
        thumbailImageView.contentMode = UIViewContentModeScaleAspectFill;
        thumbailImageView.layer.masksToBounds = YES;
        [self addSubview:thumbailImageView];
        
        imageCoverView = [[UIView alloc] initWithFrame:CGRectMake(0, thumbailImageView.frame.size.height-20, thumbailImageView.frame.size.width, 20)];
        imageCoverView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        [thumbailImageView addSubview:imageCoverView];
        
        
        playerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, imageCoverView.frame.size.width/2, imageCoverView.frame.size.height)];
        [imageCoverView addSubview:playerCountLabel];
        playerCountLabel.font =[UIFont systemFontOfSize:12];
        
        durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageCoverView.frame.size.width/2, 0, imageCoverView.frame.size.width/2, imageCoverView.frame.size.height)];
        [imageCoverView addSubview:durationLabel];
        durationLabel.font =[UIFont systemFontOfSize:12];
        durationLabel.textAlignment = NSTextAlignmentRight;
        playerCountLabel.textColor = [UIColor whiteColor];
        durationLabel.textColor = [UIColor whiteColor];
        
        
        [self addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    thumbailImageView.frame = CGRectMake(5, 5,  frame.size.width-10, self.bounds.size.height-45);
    titleLabe.frame = CGRectMake(5, self.bounds.size.height-40, frame.size.width-10, 35);
    imageCoverView.frame =CGRectMake(0, thumbailImageView.frame.size.height-20, thumbailImageView.frame.size.width, imageCoverView.frame.size.height);
    playerCountLabel.frame=CGRectMake(3, 0, imageCoverView.frame.size.width/2-3, imageCoverView.frame.size.height);
    durationLabel.frame=CGRectMake(imageCoverView.frame.size.width/2, 0, imageCoverView.frame.size.width/2-3, imageCoverView.frame.size.height);
}
-(void)setDic:(NSDictionary *)dic
{

    dicData = [dic copy];
    if(dicData && ![dicData isKindOfClass:[NSNull class]])
    {
    //显示数据
        self.hidden = NO;
        
//      titleLabe.frame = CGRectMake(5, self.bounds.size.height-40, self.bounds.size.width-10, 40);
//      thumbailImageView.hidden = NO;
        
        titleLabe.text = [dicData objectForKey:@"title"];
        playerCountLabel.text = [[NSString stringWithFormat:@"%@",[dicData objectForKey:@"view_count"]] converToPlayCount];
        durationLabel.text = [[NSString stringWithFormat:@"%@",[dicData objectForKey:@"duration"]] converToDateForDuration];
        
        NSString *imagePath =dic[@"thumbnail_v2"];
        if(imagePath == nil || [imagePath isKindOfClass:[NSNull class]])
        {
            imagePath =dic[@"thumbnail"];
        }
        if(imagePath)
        {
            [thumbailImageView setImageWithURL:[NSURL URLWithString:imagePath]];
        }
    }
    else{
                self.hidden = YES;
//        thumbailImageView.hidden = YES;
//        titleLabe.frame = CGRectInset(self.bounds, 5, 5);
//        titleLabe.text = @"加载更多";
    }
 
}
-(void)playVideo
{
    if(dicData)
    {
    VTViewController *vc =   ( VTViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc pushToPlayerWithDic:dicData];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
