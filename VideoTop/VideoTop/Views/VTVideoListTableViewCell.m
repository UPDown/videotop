//
//  VTVideoListTableViewCell.m
//  VideoTop
//
//  Created by helfy on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTVideoListTableViewCell.h"
#import "VTSingleVideoView.h"
@implementation VTVideoListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _disPlayCount = 0;
        singleViews = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
    _disPlayCount = 0;
    singleViews = [[NSMutableArray alloc] init];
}
-(void)reloadView
{
    self.disPlayCount = self.showDicArray.count;
}


-(void)setDisPlayCount:(int)newDisPlayCount
{
    float singleViewWidth = self.frame.size.width/newDisPlayCount;
    float singleViewHeight = self.cellHeigth;
    
    //新的大于旧的，初始化多出来的加载到数组中
    if(newDisPlayCount > _disPlayCount)
    {
        for (int i =_disPlayCount; i<newDisPlayCount; i++) {
            VTSingleVideoView *singleView = [[VTSingleVideoView alloc] initWithFrame:CGRectMake(singleViewWidth*i, 0, singleViewWidth, singleViewHeight)];
            [singleViews addObject:singleView];
        }
    }
    //    //新的小于旧的，
    else if(newDisPlayCount < _disPlayCount)
    {
        NSMutableArray *removeArry = [NSMutableArray array];
        for (int i =newDisPlayCount; i<self.disPlayCount; i++) {
            VTSingleVideoView * singleView = [singleViews objectAtIndex:i];
            [singleView removeFromSuperview];
            [removeArry addObject:singleView];
        }
        [singleViews removeObjectsInArray:removeArry];
    }
    _disPlayCount = newDisPlayCount;

    for (int i= 0; i< singleViews.count; i++) {
        VTSingleVideoView *singleView =  [singleViews objectAtIndex:i];
       
        if(i<self.showDicArray.count)
        {
           [singleView setDic:[self.showDicArray objectAtIndex:i]];
        }
        else{
            [singleView setDic:nil];

        }
        if(singleViews.count ==1 )
        {
            singleView.frame = CGRectMake(singleViewWidth*i, 0, singleViewWidth, singleViewHeight);
        }
        else{
            if(i%2==0)
            {
                singleView.frame = CGRectMake(singleViewWidth*i, 0, singleViewWidth, singleViewHeight);
            }
            else{
                singleView.frame = CGRectMake(singleViewWidth*i, 0, singleViewWidth, singleViewHeight);
            }
        }
        
        [self addSubview:singleView];
    }
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
