//
//  main.m
//  VideoTop
//
//  Created by HDJ on 14-6-9.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VTAppDelegate class]));
    }
}
