//
//  FileManager.h
//  QQImageWall
//
//  Created by helfy  on 12-8-14.
//
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
+(void)saveJsonStr:(NSString *)jsonStr name:(NSString *)nameStr;
+(NSString *)jsonStrWithName:(NSString *)nameStr;
+ (BOOL)writeImage:(UIImage*)image toFileAtPath:(NSString*)aPath;
+(NSString *)getRecodePlistPath:(NSString *)cataolog;
@end
