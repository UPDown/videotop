//
//  VTStringConver.h
//  VideoTop
//
//  Created by helfy on 14-6-11.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VTStringConver)
-(NSString *)converToDateForDuration;
-(NSString *)converToPlayCount;
@end
