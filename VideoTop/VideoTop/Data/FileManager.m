//
//  FileManager.m
//  QQImageWall
//
//  Created by helfy  on 12-8-14.
//
//

#import "FileManager.h"


#define KDOCUMENT_NAME @"Library/Caches/CacheLists"
#define KRECORDE_NAME @"Library/Caches/RecordeList"
@implementation FileManager
+(void)saveJsonStr:(NSString *)jsonStr name:(NSString *)nameStr
{
    if (nameStr) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentPath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:KDOCUMENT_NAME]];
        
        BOOL isDir = FALSE;
        BOOL isDirExist = [fileManager   fileExistsAtPath:documentPath isDirectory:&isDir];
        
        if(!(isDirExist && isDir))
        {
            BOOL bCreateDir = [fileManager createDirectoryAtPath:documentPath withIntermediateDirectories:YES attributes:nil error:nil];
            if(!bCreateDir){
                NSLog(@"Create Audio Directory Failed.");
            }
        }
        NSString *filePath = [documentPath stringByAppendingPathComponent:nameStr];
        
        if(![fileManager fileExistsAtPath:filePath])
        {
            [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
        }
        [jsonStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
}

+(NSString *)jsonStrWithName:(NSString *)nameStr
{
    NSString *str = nil;
    if (nameStr) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentPath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:KDOCUMENT_NAME]] ;
        
        NSString *filePath = [documentPath stringByAppendingPathComponent:nameStr];
        
        if(![fileManager fileExistsAtPath:filePath])
        {
            [fileManager createFileAtPath:filePath contents:NULL attributes:NULL];
        }
        str = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
    }
    return str;
}

+ (BOOL)writeImage:(UIImage*)image toFileAtPath:(NSString*)aPath
{
    if ((image == nil) || (aPath == nil) || ([aPath isEqualToString:@""]))
        return NO;
    
    NSFileManager *fileM = [NSFileManager defaultManager];
    NSString *fileDirectory = [aPath stringByDeletingLastPathComponent];
    
    if (![fileM fileExistsAtPath:fileDirectory]) {
        if ([fileM createDirectoryAtPath:fileDirectory withIntermediateDirectories:YES attributes:nil error:nil]==NO) {
            NSLog(@"文件夹创建失败");
        }
    }
    
    @try
    {
        NSData *imageData = nil;
        NSString *ext = [aPath pathExtension];
        if ([ext isEqualToString:@"png"])
            imageData = UIImagePNGRepresentation(image);
        else
        {
            // the rest, we write to jpeg
            // 0. best, 1. lost. about compress.
            imageData = UIImageJPEGRepresentation(image, 0);
        }
        if ((imageData == nil) || ([imageData length] <= 0))
            
            return NO;
        [imageData writeToFile:aPath atomically:YES];
        return YES;
    }
    @catch (NSException *e)
    
    {
        NSLog(@"create thumbnail exception.");
    }
    return NO;
}

+(NSString *)getRecodePlistPath:(NSString *)cataolog
{
    NSString *aPath = [[NSHomeDirectory() stringByAppendingPathComponent:KRECORDE_NAME] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",cataolog]];
    NSFileManager *fileM = [NSFileManager defaultManager];

    NSString *fileDirectory = [aPath stringByDeletingLastPathComponent];
    
    if (![fileM fileExistsAtPath:fileDirectory]) {
        if ([fileM createDirectoryAtPath:fileDirectory withIntermediateDirectories:YES attributes:nil error:nil]==NO) {
            NSLog(@"文件夹创建失败");
        }
    }
    if(![fileM fileExistsAtPath:aPath])
    {
        [fileM createFileAtPath:aPath contents:NULL attributes:NULL];
    }
    
    return aPath;
}
@end
