//
//  VTStringConver.m
//  VideoTop
//
//  Created by helfy on 14-6-11.
//  Copyright (c) 2014年 VT. All rights reserved.
//

#import "VTStringConver.h"

@implementation NSString (VTStringConver)
-(NSString *)converToDateForDuration
{
    int duration = [self floatValue];
    
    int fen,miao;
    
    fen = (duration)/60;
    miao = duration%60;
   NSString *timeS =@"";
    if(fen > 0)
    {
        timeS = [timeS stringByAppendingFormat:@"%i'",fen];
    }
    
    if(miao >0)
    {
        timeS = [timeS stringByAppendingFormat:@"%i\"",miao];
    
    }
    return timeS;
}
-(NSString *)converToPlayCount
{
    int count = [self intValue];
    
    NSString *returnStr = @"播放:";
    if(count >= 10000 && count < 100000000)
    {
        returnStr = [returnStr stringByAppendingString:[NSString stringWithFormat:@"%.1f万",count/10000.0]];
    }
    else if(count >= 100000000)
    {
        returnStr = [returnStr stringByAppendingString:[NSString stringWithFormat:@"%.1f亿",count/100000000.0]];
    }
    else{
         returnStr = [returnStr stringByAppendingString:[NSString stringWithFormat:@"%i",count]];
    }
    
    return returnStr;
}
@end
